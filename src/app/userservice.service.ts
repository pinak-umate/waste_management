import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  currentUser:any = {};

  constructor() { }

  getCurrentUser(){
    return this.currentUser;
  }

  setCurrentUser(user)
  {
    this.currentUser = user;
  }
}
