import { Component } from '@angular/core';
import {FormGroup , FormControl} from '@angular/forms';
import * as firebase from 'firebase';
import {UserserviceService} from '../userservice.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  myuser :any = null;
  profile = new FormGroup({
    fname: new FormControl() ,
    lname: new FormControl() ,
    email: new FormControl() ,
    mob: new FormControl() ,
    address: new FormControl() 
  });
  images:any = [];
  constructor(private userservice: UserserviceService, private router: Router){
    this.myuser = this.userservice.getCurrentUser();
    console.log(this.myuser);
    if(this.myuser == null)
    {
      this.router.navigate(['/login']);
    }
    this.profile.patchValue(this.myuser);
    this.getAllImagesOfUser();
  }

  getAllImagesOfUser()
  {
    let imageRef = "users/" + this.myuser.key + "/uploads";
    firebase.database().ref(imageRef).on('value', resp => {
      this.images = snapshotToArray(resp);
    });
  }
  update(){
      let path = "users/";
      let u = this.userservice.getCurrentUser();
      path += u.key;
      firebase.database().ref(path).update(this.profile.value);
    
  }
  logOut(){
    
  }

}

function snapshotToArray(snapshot) {
  var returnArr = [];

  snapshot.forEach(function(childSnapshot) {
      var item = childSnapshot.val();
      item.key = childSnapshot.key;

      returnArr.push(item);
  });

  return returnArr;
};
