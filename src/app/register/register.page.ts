import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm = new FormGroup({
    key: new FormControl(),
    uid: new FormControl(),
    fname : new FormControl(' ',Validators.compose([Validators.minLength(3),Validators.required,Validators.pattern('[a-zA-Z ]*')])),
    lname: new FormControl(' ',Validators.compose([Validators.minLength(3),Validators.required,Validators.pattern('[a-zA-Z ]*')])),
    email: new FormControl(' ',Validators.compose([Validators.required,Validators.pattern('[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})')])),
    mob: new FormControl(' ',[Validators.required,Validators.minLength(10),Validators.maxLength(10),Validators.pattern('[0-9]{10}')]),
    address: new FormControl(' ',[Validators.minLength(6),Validators.required]),
    password: new FormControl('',[Validators.minLength(6),Validators.required])
  });
  constructor(private router:Router) { }

  ngOnInit() {
  
  }
  register()
  {
    // console.log(this.registerForm.value);
    let email = this.registerForm.controls.email.value;
    let pwd = this.registerForm.controls.password.value;

    firebase.auth().createUserWithEmailAndPassword(email,pwd).then(
      resp => {
        console.log(resp.user.uid);
        let path = "users/";
        
        this.registerForm.controls.uid.setValue(resp.user.uid);
        console.log(this.registerForm.value);
        firebase.database().ref(path).push(this.registerForm.value);
        this.router.navigate(['/login']);
        alert("successfully Register");
        // we need to set the user in database
        //firebase.database().ref('users').push(this.registerForm.value);
        //this.router.navigate(['/']);
        
      }
    ).catch(
      error => {
        console.log(error);
        alert("Registration failed");
      }
    );
  }

  }

