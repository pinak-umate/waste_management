import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import * as firebase from 'firebase';
// Initialize Firebase
// var config = {
//   apiKey: "AIzaSyAF7frhDBcCJTUsWhotFLkwrEKYRl_w99I",
//   authDomain: "wastemanagement-b85a7.firebaseapp.com",
//   databaseURL: "https://wastemanagement-b85a7.firebaseio.com",
//   projectId: "wastemanagement-b85a7",
//   storageBucket: "wastemanagement-b85a7.appspot.com",
//   messagingSenderId: "245499164288"
// };
// firebase.initializeApp(config);


  
  var firebaseConfig = {
    apiKey: "AIzaSyApxbEbQHZC46jPpmdj39VVCWOPGJjBpis",
    authDomain: "wastemanagement-2933c.firebaseapp.com",
    databaseURL: "https://wastemanagement-2933c.firebaseio.com",
    projectId: "wastemanagement-2933c",
    storageBucket: "wastemanagement-2933c.appspot.com",
    messagingSenderId: "742471972643",
    appId: "1:742471972643:web:ae4d00127a910f10"
  };

  firebase.initializeApp(firebaseConfig);

/*
  firebase.database().ref('users').on('value', resp => {});
*/

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
