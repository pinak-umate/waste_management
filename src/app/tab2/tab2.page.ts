import { Component } from '@angular/core';
import {Geolocation, GeolocationOptions} from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import * as firebase from 'firebase';
import { FormGroup, FormControl, Validators } from '@angular/forms';

 import {UserserviceService} from '../userservice.service';
 import {Router} from '@angular/router'

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  uploadImage = new FormGroup({
    type: new FormControl(),
    caption: new FormControl(),
    image: new FormControl('', Validators.required),
    status: new FormControl('Pending'),
    lattitude: new FormControl(),
    longitude: new FormControl()
  });
  // base64image:any;

  ionViewDidLoad()
  {
    console.log(this.userservice.getCurrentUser());
    if(this.userservice.getCurrentUser() == null)
    {
      this.router.navigate(['/login']);
    }
  }
  constructor(private router: Router, private camera: Camera, private userservice: UserserviceService, private geolocation: Geolocation) {
    
   }
  //  location(){
  //    this.geolocation.getCurrentPosition().then((resp)=>{
       

  //    }).catch((error)=>{
  //      console.log('Error getting location',error );
  //    });
      // let watch = this.geolocation.watchPosition();
      // watch.subscribe((data)=>{
      //   data.coords.latitude;
      //   data.coords.longitude;
      // });
  //}

  upload()
  {
    console.log("after");
    console.log(this.uploadImage.value);
    // console.log(this.base64image);
    let user = this.userservice.getCurrentUser();
    let uploadpath = "users/" + user.key + "/uploads";
    console.log(uploadpath);
    this.geolocation.getCurrentPosition().then((resp)=>{
      this.uploadImage.controls.lattitude.setValue(resp.coords.latitude);
       this.uploadImage.controls.longitude.setValue(resp.coords.longitude);
       console.log(this.uploadImage.value);
       firebase.database().ref('users/'+user.key + '/uploads').push(this.uploadImage.value);
    this.uploadImage.reset();
    alert('Uploaded Successfully');

    }).catch((error)=>{
      console.log('Error getting location',error );
    });
    
  }
  capture()
  {
    let x:any = {};
    x.a = 10;
    x.b = 20;

    // console.log(this.userservice.getCurrentUser());
    let user = this.userservice.getCurrentUser();
    console.log(user);
    //  firebase.database().ref('users/'+ user.uid + "/uploads").push(this.uploadImage.value);
        const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      // this.base64image =  imageData;
      //'data:image/jpeg;base64,' +
      // alert("Camera opened");
      let user = this.userservice.getCurrentUser();
      console.log(user);
      let unique = Math.floor(Math.random() * 100);
      let myfilename = "test" + unique + "/";
      let storagePath = 'userimages/' + user.uid + myfilename;
      let storageRef = firebase.storage().ref(storagePath).putString('data:image/jpeg;base64,' + imageData, 'data_url',{contentType: 'image/jpeg'});
      storageRef.on('state_changed', snapshot => {
        // var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
  // console.log('Upload is ' + progress + '% done');
      }, error => {

      }, ()=>
      {
        console.log("Done");
        firebase.storage().ref(storagePath).getDownloadURL().then(suc => {
          console.log(suc);
          this.uploadImage.controls.image.setValue(suc);
        });
      });
    });
    //   firebase.storage().ref(storagePath).putString('data:image/jpeg;base64,' + imageData, 'data_url',{contentType: 'image/jpeg'}).then(success => {
    //     console.log("storage done");

    //     firebase.storage().ref(storagePath).getDownloadURL().then(suc => {
    //       console.log(suc);
    //       this.uploadImage.controls.image.setValue(suc);
    //     })
    //    // console.log(success.downloadURL);
    //   }).catch(error => {
    //     console.log(error);
    //   });
    //  }, (err) => {
    //   // Handle error
    //  });
  }

}
