import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as firebase from 'firebase';
import {Router} from '@angular/router'
import {UserserviceService} from '../userservice.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  constructor(private router: Router, private rest: UserserviceService) { }

  login()
  {
    let email = this.loginForm.controls.email.value;
    let pwd = this.loginForm.controls.password.value;
    firebase.auth().signInWithEmailAndPassword(email,pwd).then(success => {
      console.log(success.user.uid);
      firebase.database().ref('users').orderByChild('uid').equalTo(success.user.uid).on('value', resp => {
        let user = snapshotToArray(resp);
        console.log(user[0]);
        this.rest.setCurrentUser(user[0]);
        this.router.navigate(['tabs/tabs/tab1']);
      });
    }).catch(error=>{
      alert("Invalid Login")
    });
  }
  ngOnInit() {
  }

}
function snapshotToArray(snapshot) {
  var returnArr = [];

  snapshot.forEach(function(childSnapshot) {
      var item = childSnapshot.val();
      item.key = childSnapshot.key;

      returnArr.push(item);
  });

  return returnArr;
};